#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";


# use this block if you want to mount several applications on different path

use WebEng::Recipes;
use WebEng::Registration;
use WebEng::Profile;
use WebEng::Recipe;
use WebEng::NewRecipe;


use Plack::Builder;

builder {
    mount '/'      => WebEng::Recipes->to_app;
    mount '/registration'      => WebEng::Registration->to_app;
    mount '/profile'      => WebEng::Profile->to_app;
    mount '/recipe'      => WebEng::Recipe->to_app;
    mount '/newrecipe'      => WebEng::NewRecipe->to_app;
}