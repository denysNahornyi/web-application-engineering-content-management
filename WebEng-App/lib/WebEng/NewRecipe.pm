package WebEng::NewRecipe;
use Dancer2;

our $VERSION = '0.1';

get '/' => sub {
    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        my $driver   = "SQLite2";
        my $database = "db/web_eng.db";
        my $dsn = "DBI:$driver:dbname=$database";
        my $userid = "";
        my $pw = "";
        my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
            or die $DBI::errstr;

        print "Opened database successfully\n";

        my $stmt = qq(select * from user where id = ?;);
        my $sth = $dbh->prepare($stmt);
        my $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];

        $dbh->disconnect();
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    template 'newrecipe' => {
        'siteTitle'             => 'WebEng App - New Recipe',
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'uploaded'              => false
    };
};

post '/' => sub {
    my $uploaded = false;

    my $title = body_parameters->get('inputTitle');
    my $image = request->upload('inputImage');
    my $shortDescription = body_parameters->get('inputShortDescription');
    my $ingredients = body_parameters->get('inputIngredients');
    my $description = body_parameters->get('inputDescription');

    my $dir = path(config->{appdir}, 'public/images');
    mkdir $dir if not -e $dir;

    my $path = path($dir, $image->basename);
    if (-e $path) {
        print "'$path' already exists";
    }
    $image->link_to($path);

    my $imageFileName = '/images/'.$image->basename;

    my $driver   = "SQLite2";
    my $database = "db/web_eng.db";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $pw = "";
    my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
        or die $DBI::errstr;

    print "Opened database successfully\n";

    my $stmt = qq(select count(*) from recipe;);
    my $sth = $dbh->prepare($stmt);
    my $rv = $sth->execute() or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my $count = ($sth->fetchrow_array())[0];

    $stmt = qq(insert into recipe values (?,?,?,?,?,?););
    $sth = $dbh->prepare($stmt);
    $rv = $sth->execute($count+1, $title, $shortDescription, $ingredients, $description, $imageFileName) or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        $stmt = qq(select * from user where id = ?;);
        $sth = $dbh->prepare($stmt);
        $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    $dbh->disconnect();

    $uploaded = true;

    template 'newrecipe' => {
        'siteTitle'             => 'WebEng App - Profile',
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'title'                 => $title,
        'shortDescription'      => $shortDescription,
        'ingredients'           => $ingredients,
        'description'           => $description,
        'imageName'             => $image->basename,
        'uploaded'              => $uploaded
    };
};

true;
