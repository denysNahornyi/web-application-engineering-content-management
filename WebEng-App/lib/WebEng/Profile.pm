package WebEng::Profile;
use Dancer2;

our $VERSION = '0.1';

get '/:id' => sub {
    my $id = param('id');

    my $driver   = "SQLite2";
    my $database = "db/web_eng.db";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $pw = "";
    my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
        or die $DBI::errstr;

    print "Opened database successfully\n";

    my $stmt = qq(select * from user where id = ?;);
    my $sth = $dbh->prepare($stmt);
    my $rv = $sth->execute($id) or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my @row = $sth->fetchrow_array();

    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        $stmt = qq(select * from user where id = ?;);
        $sth = $dbh->prepare($stmt);
        $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    $dbh->disconnect();

    template 'profile' => {
        'siteTitle'   => 'WebEng App - Profile',
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'id'    => $row[0],
        'email'   => $row[1],
        'password'   => $row[2],
        'firstName'   => $row[3],
        'lastName'   => $row[4],
        'address'   => $row[5],
        'city'   => $row[6],
        'zipCode'   => $row[7],
        'updated' => false
    };
};

post '/:id' => sub {

    my $id = param('id');

    my $updated = false;

    my $email = body_parameters->get('inputEmail');
    my $password = body_parameters->get('inputPassword');
    my $firstName = body_parameters->get('inputFirstName');
    my $lastName = body_parameters->get('inputLastName');
    my $address = body_parameters->get('inputAddress');
    my $city = body_parameters->get('inputCity');
    my $zipCode = body_parameters->get('inputZipCode');

    my $driver   = "SQLite2";
    my $database = "db/web_eng.db";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $pw = "";
    my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
        or die $DBI::errstr;

    print "Opened database successfully\n";

    my $stmt = qq(update user set email=?, password=?, firstname=?, lastname=?, address=?, city=?, zip=? where id=?;);
    my $sth = $dbh->prepare($stmt);
    my $rv = $sth->execute($email, $password, $firstName, $lastName, $address, $city, $zipCode, $id) or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    $stmt = qq(select * from user where id = ?;);
    $sth = $dbh->prepare($stmt);
    $rv = $sth->execute($id) or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my @row = $sth->fetchrow_array();

    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        $stmt = qq(select * from user where id = ?;);
        $sth = $dbh->prepare($stmt);
        $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    $dbh->disconnect();

    $updated = true;

    template 'profile' => {
        'siteTitle'    => 'WebEng App - Profile',
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'id'    => $row[0],
        'email'   => $row[1],
        'password'   => $row[2],
        'firstName'   => $row[3],
        'lastName'   => $row[4],
        'address'   => $row[5],
        'city'   => $row[6],
        'zipCode'   => $row[7],
        'updated' => $updated
    };
};

true;
