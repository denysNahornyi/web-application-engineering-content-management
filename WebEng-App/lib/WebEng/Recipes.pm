package WebEng::Recipes;

use DBI;
use Dancer2;

our $VERSION = '0.1';
our @data;


get '/' => sub {
    my $driver   = "SQLite2";
    my $database = "db/web_eng.db";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $pw = "";
    my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
        or die $DBI::errstr;

    print "Opened database successfully\n";

    my $stmt = qq(select * from recipe;);
    my $sth = $dbh->prepare($stmt);
    my $rv = $sth->execute() or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    @data = ();

    while(my @row = $sth->fetchrow_array()) {

        my $foodItem = {
            "id" => $row[0],
            "title" => $row[1],
            "shortdescription" => $row[2],
            "ingredients" => $row[3],
            "description" => $row[4],
            "image" => $row[5],
        };

        push @data, $foodItem;
    }

    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        $stmt = qq(select * from user where id = ?;);
        $sth = $dbh->prepare($stmt);
        $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    $dbh->disconnect();


    template 'recipes' => {
        'siteTitle'             => 'WebEng App - Recipes',
        'invalidCredentials'    => false,
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'data'                  => \@data
    };
};

post "/login" => sub {

    my $inputEmail = body_parameters->get('inputLoginEmail');
    my $inputPassword = body_parameters->get('inputLoginPassword');

    my $driver   = "SQLite2";
    my $database = "db/web_eng.db";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $pw = "";
    my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
        or die $DBI::errstr;

    print "Opened database successfully\n";

    my $stmt = qq(select * from user where email = ?;);
    my $sth = $dbh->prepare($stmt);
    my $rv = $sth->execute($inputEmail) or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my $sessionEmail;
    my $sessionId;
    my $loggedIn;
    my $invalidCredentials = false;

    my @row = $sth->fetchrow_array();

    if ($row[2] eq $inputPassword) {
        session sessionId => $row[0];
        $sessionId = $row[0];
        $sessionEmail = $row[1];
        $loggedIn = true;
    } else {
        $invalidCredentials = true;
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    $dbh->disconnect();

    template 'recipes' => {
        'siteTitle'             => 'WebEng App - Recipes',
        'invalidCredentials'    => $invalidCredentials,
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'data'                  => \@data
    };

};

get "/logout" => sub {
    my $loggedIn = false;
    my $sessionEmail = '';
    my $sessionId = '';

    session sessionId => undef;

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    template 'recipes' => {
        'siteTitle'             => 'WebEng App - Recipes',
        'invalidCredentials'    => false,
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'data'                  => \@data
    };

};

true;
