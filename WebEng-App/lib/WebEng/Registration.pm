package WebEng::Registration;
use Dancer2;

our $VERSION = '0.1';

get '/' => sub {

    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        my $driver   = "SQLite2";
        my $database = "db/web_eng.db";
        my $dsn = "DBI:$driver:dbname=$database";
        my $userid = "";
        my $pw = "";
        my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
            or die $DBI::errstr;

        print "Opened database successfully\n";

        my $stmt = qq(select * from user where id = ?;);
        my $sth = $dbh->prepare($stmt);
        my $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];

        $dbh->disconnect();
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    template 'registration' => {
        'title'                 => 'WebEng App - Registration',
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'registered'            => false
    };
};

post '/' => sub {
    my $registered = false;

    my $email = body_parameters->get('inputEmail');
    my $password = body_parameters->get('inputPassword');
    my $firstName = body_parameters->get('inputFirstName');
    my $lastName = body_parameters->get('inputLastName');
    my $address = body_parameters->get('inputAddress');
    my $city = body_parameters->get('inputCity');
    my $zipCode = body_parameters->get('inputZipCode');

    my $driver   = "SQLite2";
    my $database = "db/web_eng.db";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $pw = "";
    my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
        or die $DBI::errstr;

    print "Opened database successfully\n";

    my $stmt = qq(select count(*) from user;);
    my $sth = $dbh->prepare($stmt);
    my $rv = $sth->execute() or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my $count = ($sth->fetchrow_array())[0];

    $stmt = qq(insert into user values (?,?,?,?,?,?,?,?););
    $sth = $dbh->prepare($stmt);
    $rv = $sth->execute($count+1, $email, $password, $firstName, $lastName, $address, $city, $zipCode) or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        $stmt = qq(select * from user where id = ?;);
        $sth = $dbh->prepare($stmt);
        $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    $dbh->disconnect();

    $registered = true;

    template 'registration' => {
        'siteTitle'    => 'WebEng App - Registration',
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
        'registered' => $registered
    };
};

true;
