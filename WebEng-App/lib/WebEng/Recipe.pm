package WebEng::Recipe;
use Dancer2;

our $VERSION = '0.1';

get '/:id' => sub {
    my $id = param('id');

    my $driver   = "SQLite2";
    my $database = "db/web_eng.db";
    my $dsn = "DBI:$driver:dbname=$database";
    my $userid = "";
    my $pw = "";
    my $dbh = DBI->connect($dsn, $userid, $pw, { RaiseError => 1 })
        or die $DBI::errstr;

    print "Opened database successfully\n";

    my $stmt = qq(select * from recipe where id = ?;);
    my $sth = $dbh->prepare($stmt);
    my $rv = $sth->execute($id) or die $DBI::errstr;

    if($rv < 0) {
        print $DBI::errstr;
    }

    my $title;
    my $shortdescription;
    my $ingredients;
    my $description;
    my $image;

    my @row = $sth->fetchrow_array();

    $title = $row[1];
    $shortdescription = $row[2];
    $ingredients = $row[3];
    $description = $row[4];
    $image = $row[5];

    my $sessionId = session->read('sessionId');
    my $loggedIn = false;
    my $sessionEmail = '';

    if(defined $sessionId) {
        $stmt = qq(select * from user where id = ?;);
        $sth = $dbh->prepare($stmt);
        $rv = $sth->execute($sessionId) or die $DBI::errstr;

        if($rv < 0) {
            print $DBI::errstr;
        }

        my @row = $sth->fetchrow_array();

        $loggedIn = true;
        $sessionEmail = $row[1];
    }

    print "sessionId: $sessionId, sessionEmail: $sessionEmail, loggedIn: $loggedIn";

    $dbh->disconnect();

    template 'recipe' => {
        'siteTitle'    => 'WebEng App - '.$title,
        'id' => $id,
        'title' => $title,
        'shortdescription' => $shortdescription,
        'ingredients' => $ingredients,
        'description' => $description,
        'image' => $image,
        'loggedIn'              => $loggedIn,
        'sessionEmail'          => $sessionEmail,
        'sessionId'             => $sessionId,
    };
};

true;
