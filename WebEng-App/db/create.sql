drop table user;

create table user (id integer primary key, email text not null, password text not null, firstname text not null, lastname text not null, address text not null, city text not null, zip text not null);

drop table recipe;

create table recipe (id integer primary key, title text not null, shortdescription text not null, ingredients text not null, description text not null, image text not null);
